globals
[
  selected
  initial-segregation
  color-triangle
  color-square
  setup-run?
  world-size
  population-density
  red-blue-ratio
  interactive?
  wiggle?
]

breed [ people person ]

people-own [
  unhappy?
  original-patch
  new-patch
  init-heading
  xcor-stable
  ycor-stable
]

;;
;; SETUP PROCEDURES
;;

to setup                          ;; executed when we press the SETUP button
  clear-all                       ;; clear all patches and turtles

  set world-size 10
  set population-density 85
  set red-blue-ratio 0.5

  resize-world 0 ( world-size - 1 ) 0 ( world-size - 1 )
  set-patch-size 600 / world-size

  set color-triangle rgb 109 171 203 ;blue
  set color-square rgb 234 191 70 ;red

  let n-people precision ( population-density / 100 *  world-size ^ 2 ) 0
  create-people n-people
  [
    move-to one-of patches with [ not any? people-here ]
    ifelse random-float 1 < red-blue-ratio
    [
      set color color-square
    ] [
      set color color-triangle
    ]
    set xcor-stable xcor
    set ycor-stable ycor
  ]

  ask people
  [
    update-happiness
    if ( unhappy? )
    [
      set heading -15 + random 30
      set init-heading heading
    ]
    adjust-shapes
  ]
  set selected nobody
  set initial-segregation 100 * sum [ count-same-neighbors ] of people / sum [ count-all-neighbors ] of people

  set setup-run? true

  reset-ticks
end


;;
;; GO PROCEDURES
;;

to go
  ask people
  [
    update-happiness
    adjust-shapes
    if ( wiggle? ) [
     wiggle
    ]
  ]
  ifelse ( interactive? )
  [
    ifelse mouse-down?
    [
      ; if the mouse is down then handle selecting and dragging
      handle-select-and-drag
    ] [
      ; otherwise, make sure the previous selection is deselected
      if ( selected != nobody )
      [
        ask selected
        [
          move-to min-one-of patches [ sqrt ( ( mouse-xcor - pxcor ) ^ 2 + ( mouse-ycor - pycor ) ^ 2 ) ]
          if ( any? other people-here ) [
            move-to original-patch
          ]
          set xcor-stable pxcor
          set ycor-stable pycor
        ]
      ]
      set selected nobody
      reset-perspective
      if ( count people with [ unhappy? ] = 0 and not mouse-down? )
      [
        set setup-run? false
        stop
      ]
    ]
  ] [
    ifelse ( count people with [ unhappy? ] > 0 )
    [
      ask one-of people with [ unhappy? ]
      [
        move-to one-of patches with [ not any? people-here ]
      ]
    ] [
      set setup-run? false
      stop
    ]
  ]
  tick
end

;;
;; OTHER METHODS
;;

to-report count-same-neighbors
  report count ( people-on neighbors ) with [ color = [ color ] of myself ]
end

to-report count-all-neighbors
  report count people-on neighbors
end

to update-happiness
  ifelse ( count-all-neighbors = 0 )
  [
    ; person has no neighbors at all -> also happy
    set unhappy? false
  ] [
    ifelse ( count-same-neighbors / count-all-neighbors < ( %-similar-wanted / 100 )  )
    [
      set unhappy? true
    ] [
      set unhappy? false
    ]
  ]
end

to-report happy-state
  ifelse ( count people with [ unhappy? ] > 0 )
  [
    report ( word "Still " count people with [ unhappy? ] " people unhappy." )
  ] [
    report "Hurray, everybody is happy!"
  ]
end

to adjust-shapes
  ifelse ( color = color-triangle )
  [
    ifelse ( unhappy? )
    [
      set shape "triangle_sad"
    ] [
      set shape "triangle_happy"
      set heading 0
    ]
  ] [
    ifelse ( unhappy? )
    [
      set shape "square_sad"
    ] [
      set shape "square_happy"
      set heading 0
    ]
  ]
end

to wiggle
  if ( unhappy? )
  [
    set heading 15 * sin ( 90 * ticks / pi + init-heading * 90 )
    setxy ( xcor-stable + random-float 0.1 - 0.05 ) ( ycor-stable + random-float 0.1 - 0.05 )
  ]
end

to handle-select-and-drag
  ; if no turtle is selected
  ifelse selected = nobody
  [
    ; pick the closet turtle
    set selected min-one-of turtles [distancexy mouse-xcor mouse-ycor]
    ; check whether or not it's close enough
    ifelse ( [distancexy mouse-xcor mouse-ycor] of selected > 1 or [ not unhappy? ] of selected )
    [
      set selected nobody ; if not, don't select it
    ] [
      watch selected ; if it is, go ahead and `watch` it
      ask selected [
        set original-patch patch-here
      ]
    ]
  ] [
    ; if a turtle is selected, move it to the mouse
    ask selected
    [
      setxy mouse-xcor mouse-ycor
      set xcor-stable mouse-xcor
      set ycor-stable mouse-ycor
    ]
  ]
end
@#$#@#$#@
GRAPHICS-WINDOW
5
10
613
619
-1
-1
60.0
1
10
1
1
1
0
0
0
1
0
9
0
9
1
1
1
ticks
15.0

BUTTON
620
625
725
658
New game
setup
NIL
1
T
OBSERVER
NIL
S
NIL
NIL
1

SLIDER
620
670
955
703
%-similar-wanted
%-similar-wanted
0
100
33.0
1
1
%
HORIZONTAL

PLOT
620
10
955
405
Segregation
NIL
%
0.0
10.0
0.0
100.0
false
false
"set-plot-x-range 0 max list 1 ticks \nset-plot-y-range floor initial-segregation 100" "set-plot-x-range 0 max list 1 ticks \nset-plot-y-range floor initial-segregation 100"
PENS
"default" 1.0 0 -16777216 true "" "plotxy ticks 100 * sum [ count-same-neighbors ] of people / sum [ count-all-neighbors ] of people"

MONITOR
300
625
615
706
Percentage of similar neighbors
100 * sum [ count-same-neighbors ] of people / sum [ count-all-neighbors ] of people
2
1
20

MONITOR
5
625
295
706
 (Un)happy people
happy-state
17
1
20

BUTTON
735
625
840
658
Play!
set interactive? true\nset wiggle? true\ngo
T
1
T
OBSERVER
NIL
P
NIL
NIL
1

BUTTON
850
625
955
658
Play for me!
set interactive? false\nset wiggle? false\ngo
T
1
T
OBSERVER
NIL
M
NIL
NIL
1

TEXTBOX
625
415
955
435
Instructions
17
75.0
1

TEXTBOX
625
440
970
546
Every shape is happy, when it has a certain degree of similar shapes in its neighbor- hood (defined by                             ). \nTry to move each shape until everyone is happy.\n
16
0.0
1

TEXTBOX
625
550
775
570
Rules
17
75.0
1

TEXTBOX
625
575
955
616
- Move shapes randomly\n- Only unhappy shapes can be moved
16
0.0
1

TEXTBOX
760
480
910
496
%-similar-wanted
16
105.0
1

@#$#@#$#@
## WHAT IS IT?

This code example shows how to use shapes to create animations.  In this example, there is a walking person and growing flowers.

## HOW IT WORKS

A counter is used to increment the shape to be used in each animation.

For the person, each progressive frame of the person walking has a different shape.  There are 9 shapes total.  Each progressive shape is numbered as person_#, where the # is one higher then the shape that came before.  After final numbered shape (person_9), the shapes repeat from the start (person_1) and continue to cycle through the remaining shapes.  The person is moved forward a small bit with each new shape, to give the illusion of walking.

For the flowers, each progressive frame of their growth shows the development of the flower from a small seedling to a fully grown flower.  Each frame has a new shape.  Each progressive shape is numbered as flower_#, where the # is one higher then the shape that came before.  After final numbered shape (flower_16), the shapes stop incrementing and the flower is shown as fully grown.

## NETLOGO FEATURES

The model has a frame rate setting of 15 frames per second, for smooth animation that isn't too fast.  The speed can be further adjusted by the user using the speed slider.

<!-- 2005 -->
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
true
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
true
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
true
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 150 300 165 300 165 285 165 270

flower-1
false
0
Polygon -10899396 true false 150 300 165 300 165 285 165 270

flower-10
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240
Polygon -10899396 true false 165 150 150 150 135 135 135 105 150 105 165 135
Polygon -7500403 true true 135 120 150 135 135 105

flower-11
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240
Polygon -10899396 true false 150 135 120 120 120 105 135 90 150 90 165 120
Polygon -7500403 true true 150 90 150 120 135 90
Polygon -7500403 true true 120 105 120 120 150 120
Line -7500403 true 150 90 150 120

flower-12
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -10899396 true false 133 105 32
Circle -10899396 true false 152 90 28
Circle -10899396 true false 118 88 32
Circle -10899396 true false 133 73 32
Circle -7500403 true true 125 80 50
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240
Circle -16777216 true false 147 102 6

flower-13
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -10899396 true false 133 120 32
Circle -10899396 true false 167 90 28
Circle -10899396 true false 103 88 32
Circle -10899396 true false 133 58 32
Circle -7500403 true true 120 75 60
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240
Circle -16777216 true false 144 99 12

flower-14
false
0
Circle -10899396 true false 103 58 32
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -10899396 true false 103 120 32
Circle -7500403 true true 133 135 32
Circle -7500403 true true 182 90 28
Circle -10899396 true false 167 60 28
Circle -10899396 true false 167 122 28
Circle -7500403 true true 88 88 32
Circle -7500403 true true 133 43 32
Circle -7500403 true true 105 60 90
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240
Circle -16777216 true false 135 90 30

flower-15
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 90 137 28
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 90 45 28
Circle -7500403 true true 182 45 28
Circle -7500403 true true 182 137 28
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 120 75 60
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

flower-16
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

flower-2
false
0
Polygon -10899396 true false 150 300 180 255 180 270 165 300

flower-3
false
0
Polygon -10899396 true false 150 300 180 240 195 225 195 240 165 300

flower-4
false
0
Polygon -10899396 true false 150 300 180 240 180 210 195 225 195 240 165 300
Polygon -10899396 true false 180 255 165 240 150 240

flower-5
false
0
Polygon -10899396 true false 150 300 180 240 180 210 180 165 195 195 195 240 165 300
Polygon -10899396 true false 180 255 135 225 105 240 135 240

flower-6
false
0
Polygon -10899396 true false 150 300 180 240 180 210 165 165 165 150 195 195 195 240 165 300
Polygon -10899396 true false 180 255 135 210 120 210 90 225 105 240 135 240
Polygon -10899396 true false 185 235 210 210 222 208 210 225

flower-7
false
0
Polygon -10899396 true false 180 255 150 210 105 210 83 241 135 240
Polygon -10899396 true false 150 300 180 240 180 210 165 150 150 135 165 135 195 195 195 240 165 300
Polygon -10899396 true false 189 230 217 200 235 195 255 195 232 210

flower-8
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Polygon -10899396 true false 189 233 219 188 240 180 255 195 228 214
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240
Polygon -10899396 true false 150 135 135 135 135 120 150 120

flower-9
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Polygon -10899396 true false 189 233 219 188 240 180 270 195 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240
Polygon -10899396 true false 150 135 135 120 135 105 150 105
Line -7500403 true 135 105 150 120

flower12
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

man standing
false
0
Circle -13345367 true false 112 23 75
Rectangle -13345367 true false 136 91 164 209
Polygon -13345367 true false 136 107 106 107 91 149 105 149 120 121 136 121 181 121 195 150 211 150 195 106
Polygon -13345367 true false 136 209 106 225 106 255 90 255 90 269 121 269 121 240 150 225 180 240 180 269 211 269 211 256 196 256 196 225 165 210

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person-1
false
0
Polygon -7500403 true true 120 195 120 105 135 90 195 90 195 105 195 120 180 180
Circle -7500403 true true 125 5 80
Rectangle -7500403 true true 147 73 183 90
Polygon -7500403 true true 165 195 120 180 96 228 60 255 75 300 133 242
Polygon -7500403 true true 180 180 120 180 156 232 159 299 199 299 195 223
Polygon -7500403 true true 135 90 95 125 60 180 90 195 127 137 150 120
Polygon -7500403 true true 180 136 180 151 206 202 234 190 210 136 195 91

person-2
false
0
Polygon -7500403 true true 180 136 165 166 192 203 220 191 202 147 191 98
Circle -7500403 true true 125 5 80
Rectangle -7500403 true true 147 73 183 90
Polygon -7500403 true true 168 176 117 160 105 225 75 255 90 300 135 255
Polygon -7500403 true true 174 164 120 180 150 225 150 300 195 300 195 225
Polygon -7500403 true true 105 180 120 105 135 90 187 89 195 105 195 120 180 180
Polygon -7500403 true true 131 91 96 135 75 180 120 195 135 150 150 120

person-3
false
0
Polygon -7500403 true true 171 123 181 162 165 200 196 201 198 150 194 104
Polygon -7500403 true true 165 180 120 180 120 225 90 285 135 300 165 225
Polygon -7500403 true true 180 180 121 180 158 239 135 300 180 300 180 225
Circle -7500403 true true 125 5 80
Rectangle -7500403 true true 147 73 183 90
Polygon -7500403 true true 116 192 120 105 135 90 180 90 195 105 195 120 180 195
Polygon -7500403 true true 135 90 120 105 93 193 128 199 135 150 150 120

person-4
false
0
Polygon -7500403 true true 180 135 180 165 175 205 186 203 190 135 183 89
Circle -7500403 true true 125 5 80
Rectangle -7500403 true true 147 73 183 90
Polygon -7500403 true true 165 180 120 180 120 225 120 300 165 300 165 225
Polygon -7500403 true true 181 182 151 182 136 227 136 302 181 302 181 212
Polygon -7500403 true true 120 190 120 105 130 90 180 90 195 105 195 120 180 195
Polygon -7500403 true true 145 90 130 102 120 135 120 165 120 210 150 210

person-5
false
0
Circle -7500403 true true 125 5 80
Rectangle -7500403 true true 147 73 183 90
Polygon -7500403 true true 180 180 135 180 135 225 120 300 180 300 180 225
Polygon -7500403 true true 195 180 120 180 135 225 150 300 195 300 201 222
Polygon -7500403 true true 120 195 120 105 135 90 180 90 195 105 195 120 180 195
Polygon -7500403 true true 180 135 180 165 180 195 195 195 195 120 183 89
Polygon -7500403 true true 136 128 136 158 136 203 166 203 165 120 142 95

person-6
false
0
Polygon -7500403 true true 165 180 120 180 122 222 105 300 150 300 165 225
Polygon -7500403 true true 182 174 120 178 165 240 173 298 214 294 210 225
Circle -7500403 true true 125 5 80
Rectangle -7500403 true true 147 73 183 90
Polygon -7500403 true true 116 192 120 105 135 90 180 90 195 105 195 120 180 195
Polygon -7500403 true true 135 90 105 120 93 193 120 195 120 135 150 120
Polygon -7500403 true true 150 124 150 154 161 201 195 199 193 133 183 93

person-7
false
0
Circle -7500403 true true 125 5 80
Rectangle -7500403 true true 147 73 183 90
Polygon -7500403 true true 165 180 120 180 105 240 90 300 135 300 150 240
Polygon -7500403 true true 165 166 120 181 177 229 180 297 221 289 213 210
Polygon -7500403 true true 120 195 120 105 135 90 180 90 195 105 195 120 189 193
Polygon -7500403 true true 122 103 100 133 79 195 111 202 124 165 154 120
Polygon -7500403 true true 180 133 165 163 180 210 210 195 198 146 195 105

person-8
false
0
Polygon -7500403 true true 183 168 138 183 178 239 203 300 240 280 213 226
Circle -7500403 true true 125 5 80
Rectangle -7500403 true true 147 73 183 90
Polygon -7500403 true true 120 180 120 105 135 90 180 90 195 105 195 120 195 180
Polygon -7500403 true true 135 90 99 127 66 183 92 197 118 158 150 120
Polygon -7500403 true true 180 133 165 163 210 195 232 179 203 141 183 87
Polygon -7500403 true true 165 180 120 180 105 240 75 300 120 300 150 240

person-9
false
0
Circle -7500403 true true 125 5 80
Rectangle -7500403 true true 147 73 183 90
Polygon -7500403 true true 165 180 120 180 105 225 68 289 105 300 150 225
Polygon -7500403 true true 188 185 135 180 163 240 171 300 217 301 200 229
Polygon -7500403 true true 121 180 121 105 136 90 181 90 195 97 196 120 187 185
Polygon -7500403 true true 135 90 90 135 60 180 90 195 120 150 150 120
Polygon -7500403 true true 185 137 185 154 222 199 246 179 215 139 185 92

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

square_happy
true
0
Rectangle -7500403 true true 30 30 270 270
Circle -16777216 true false 116 124 16
Circle -16777216 true false 163 117 20
Polygon -16777216 true false 84 190 106 205 124 212 144 214 170 211 187 204 204 191 211 182 209 179 207 174 203 170 197 176 187 185 171 194 156 199 142 201 127 200 108 196 94 187 90 185 87 187

square_sad
true
0
Rectangle -7500403 true true 30 30 270 270
Circle -16777216 true false 116 124 16
Circle -16777216 true false 163 117 20
Polygon -16777216 true false 84 200 106 185 124 178 144 176 170 179 187 186 204 199 211 208 209 211 207 216 203 220 197 214 187 205 171 196 156 191 142 189 127 190 108 194 94 203 90 205 87 203

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

triangle_happy
true
0
Polygon -7500403 true true 150 30 15 255 285 255
Circle -16777216 true false 116 124 16
Circle -16777216 true false 163 117 20
Polygon -16777216 true false 84 190 106 205 124 212 144 214 170 211 187 204 204 191 211 182 209 179 207 174 203 170 197 176 187 185 171 194 156 199 142 201 127 200 108 196 94 187 90 185 87 187

triangle_sad
true
0
Polygon -7500403 true true 150 30 15 255 285 255
Circle -16777216 true false 116 124 16
Circle -16777216 true false 163 117 20
Polygon -16777216 true false 84 200 106 185 124 178 144 176 170 179 187 186 204 199 211 208 209 211 207 216 203 220 197 214 187 205 171 196 156 191 142 189 127 190 108 194 94 203 90 205 87 203

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.2.0
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
1
@#$#@#$#@
